namespace GymManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SubscriptionOffer")]
    public partial class SubscriptionOffer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SubscriptionOffer()
        {
            Subscription = new HashSet<Subscription>();
        }

        public int id { get; set; }

        [Column(TypeName = "date")]
        public DateTime beginDate { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        public int? duration { get; set; }

        [Column(TypeName = "date")]
        public DateTime? endDate { get; set; }

        [Required]
        [StringLength(255)]
        public string name { get; set; }

        public double price { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Subscription> Subscription { get; set; }
    }
}
