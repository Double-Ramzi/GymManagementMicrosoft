namespace GymManagement.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Card> Card { get; set; }
        public virtual DbSet<HealthRecord> HealthRecord { get; set; }
        public virtual DbSet<Member> Member { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Staff> Staff { get; set; }
        public virtual DbSet<Subscription> Subscription { get; set; }
        public virtual DbSet<SubscriptionOffer> SubscriptionOffer { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.salt)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.userName)
                .IsUnicode(false);

            modelBuilder.Entity<HealthRecord>()
                .Property(e => e.confidential)
                .IsFixedLength();

            modelBuilder.Entity<HealthRecord>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Member>()
                .HasMany(e => e.HealthRecord)
                .WithOptional(e => e.Member)
                .HasForeignKey(e => e.member_id);

            modelBuilder.Entity<Member>()
                .HasMany(e => e.Subscription)
                .WithOptional(e => e.Member)
                .HasForeignKey(e => e.member_id);

            modelBuilder.Entity<Person>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.firstName)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.gender)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.lastName)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.phoneEmergency)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.phoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .HasMany(e => e.Account)
                .WithRequired(e => e.Person)
                .HasForeignKey(e => e.person_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                .HasMany(e => e.Card)
                .WithOptional(e => e.Person)
                .HasForeignKey(e => e.person_id);

            modelBuilder.Entity<Person>()
                .HasOptional(e => e.Member)
                .WithRequired(e => e.Person);

            modelBuilder.Entity<Person>()
                .HasOptional(e => e.Staff)
                .WithRequired(e => e.Person);

            modelBuilder.Entity<Staff>()
                .HasMany(e => e.HealthRecord)
                .WithOptional(e => e.Staff)
                .HasForeignKey(e => e.staff_id);

            modelBuilder.Entity<SubscriptionOffer>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<SubscriptionOffer>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<SubscriptionOffer>()
                .HasMany(e => e.Subscription)
                .WithOptional(e => e.SubscriptionOffer)
                .HasForeignKey(e => e.subscriptionOffer_id);
        }
    }
}
