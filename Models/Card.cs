namespace GymManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Card")]
    public partial class Card
    {
        public int id { get; set; }

        public int physicalNumber { get; set; }

        public int? person_id { get; set; }

        public virtual Person Person { get; set; }
    }
}
