namespace GymManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Subscription")]
    public partial class Subscription
    {
        public int id { get; set; }

        [Column(TypeName = "date")]
        public DateTime subscriptionDate { get; set; }

        public int? member_id { get; set; }

        public int? subscriptionOffer_id { get; set; }

        public virtual Member Member { get; set; }

        public virtual SubscriptionOffer SubscriptionOffer { get; set; }
    }
}
