namespace GymManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Person")]
    public partial class Person
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Person()
        {
            Account = new HashSet<Account>();
            Card = new HashSet<Card>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string address { get; set; }

        [Column(TypeName = "date")]
        public DateTime birthdate { get; set; }

        [Required]
        [StringLength(255)]
        public string email { get; set; }

        [Required]
        [StringLength(255)]
        public string firstName { get; set; }

        [Required]
        [StringLength(255)]
        public string gender { get; set; }

        [Required]
        [StringLength(255)]
        public string lastName { get; set; }

        [StringLength(255)]
        public string phoneEmergency { get; set; }

        [Required]
        [StringLength(255)]
        public string phoneNumber { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Card> Card { get; set; }

        public virtual Member Member { get; set; }

        public virtual Staff Staff { get; set; }
    }
}
