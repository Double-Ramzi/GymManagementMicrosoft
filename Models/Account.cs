namespace GymManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Account")]
    public partial class Account
    {
        public int id { get; set; }

        public int accountType { get; set; }

        [Required]
        [StringLength(255)]
        public string password { get; set; }

        [Required]
        [StringLength(255)]
        public string salt { get; set; }

        [Required]
        [StringLength(255)]
        public string userName { get; set; }

        public int person_id { get; set; }

        public virtual Person Person { get; set; }
    }
}
