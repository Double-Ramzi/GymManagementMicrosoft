namespace GymManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HealthRecord")]
    public partial class HealthRecord
    {
        public int id { get; set; }

        [Required]
        [MaxLength(1)]
        public byte[] confidential { get; set; }

        [Column(TypeName = "date")]
        public DateTime? date { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        public int? member_id { get; set; }

        public int? staff_id { get; set; }

        public virtual Staff Staff { get; set; }

        public virtual Member Member { get; set; }
    }
}
