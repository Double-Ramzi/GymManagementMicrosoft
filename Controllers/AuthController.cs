﻿using GymManagement.Manager;
using GymManagement.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;

namespace GymManagement.Controllers
{
    public class AuthController : Controller
    {
        private Model1 db = new Model1();
        private UserManager userManager;

        public AuthController()
        {
            this.userManager = new UserManager(this.db);
        }

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register(FormCollection collection)
        {
            if (collection["userName"] != null && collection["password"] != null)
            {
                Person person = new Person();
                person.firstName = collection["Person.firstName"];
                person.lastName = collection["Person.lastName"];
                person.birthdate = new DateTime();
                person.gender = collection["Person.gender"];
                person.address = ".";
                person.email = collection["Person.email"];
                person.phoneNumber = ".";
                person.phoneEmergency = ".";

                db.Person.Add(person);

                Account account = new Account();
                account.userName = collection["userName"];
                account.password = collection["password"];
                account.accountType = 0;
                account.salt = ".";
                account.Person = person;

                userManager.Register(account);
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public ActionResult SignIn(FormCollection collection)
        {
            if (collection["userName"] != null && collection["password"] != null)
            {
                Account account = userManager.Validate(collection["userName"], collection["password"]);

                if (account == null)
                    return View();

                userManager.SignIn(System.Web.HttpContext.Current, account);

                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public ActionResult SignOut()
        {
            userManager.SignOut(System.Web.HttpContext.Current);

            return RedirectToAction("Index", "Home");
        }
    }
}