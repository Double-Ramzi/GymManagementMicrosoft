﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GymManagement.Filters;
using GymManagement.Models;

namespace GymManagement.Controllers
{
    [AuthActionFilter]
    public class HealthRecordsController : Controller
    {
        private Model1 db = new Model1();

        // GET: HealthRecords
        public ActionResult Index()
        {
            var healthRecord = db.HealthRecord.Include(h => h.Member).Include(h => h.Staff);
            return View(healthRecord.ToList());
        }

        // GET: HealthRecords/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HealthRecord healthRecord = db.HealthRecord.Find(id);
            if (healthRecord == null)
            {
                return HttpNotFound();
            }
            return View(healthRecord);
        }

        // GET: HealthRecords/Create
        public ActionResult Create()
        {
            ViewBag.member_id = new SelectList(db.Member, "id", "id");
            ViewBag.staff_id = new SelectList(db.Staff, "id", "id");
            return View();
        }

        // POST: HealthRecords/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,confidential,date,description,member_id,staff_id")] HealthRecord healthRecord)
        {
            if (ModelState.IsValid)
            {
                db.HealthRecord.Add(healthRecord);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.member_id = new SelectList(db.Member, "id", "id", healthRecord.member_id);
            ViewBag.staff_id = new SelectList(db.Staff, "id", "id", healthRecord.staff_id);
            return View(healthRecord);
        }

        // GET: HealthRecords/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HealthRecord healthRecord = db.HealthRecord.Find(id);
            if (healthRecord == null)
            {
                return HttpNotFound();
            }
            ViewBag.member_id = new SelectList(db.Member, "id", "id", healthRecord.member_id);
            ViewBag.staff_id = new SelectList(db.Staff, "id", "id", healthRecord.staff_id);
            return View(healthRecord);
        }

        // POST: HealthRecords/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,confidential,date,description,member_id,staff_id")] HealthRecord healthRecord)
        {
            if (ModelState.IsValid)
            {
                db.Entry(healthRecord).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.member_id = new SelectList(db.Member, "id", "id", healthRecord.member_id);
            ViewBag.staff_id = new SelectList(db.Staff, "id", "id", healthRecord.staff_id);
            return View(healthRecord);
        }

        // GET: HealthRecords/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HealthRecord healthRecord = db.HealthRecord.Find(id);
            if (healthRecord == null)
            {
                return HttpNotFound();
            }
            return View(healthRecord);
        }

        // POST: HealthRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HealthRecord healthRecord = db.HealthRecord.Find(id);
            db.HealthRecord.Remove(healthRecord);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
