﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GymManagement.Filters;
using GymManagement.Models;

namespace GymManagement.Controllers
{
    [AuthActionFilter]
    public class SubscriptionOffersController : Controller
    {
        private Model1 db = new Model1();

        // GET: SubscriptionOffers
        public ActionResult Index()
        {
            return View(db.SubscriptionOffer.ToList());
        }

        // GET: SubscriptionOffers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubscriptionOffer subscriptionOffer = db.SubscriptionOffer.Find(id);
            if (subscriptionOffer == null)
            {
                return HttpNotFound();
            }
            return View(subscriptionOffer);
        }

        // GET: SubscriptionOffers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SubscriptionOffers/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,beginDate,description,duration,endDate,name,price")] SubscriptionOffer subscriptionOffer)
        {
            if (ModelState.IsValid)
            {
                db.SubscriptionOffer.Add(subscriptionOffer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(subscriptionOffer);
        }

        // GET: SubscriptionOffers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubscriptionOffer subscriptionOffer = db.SubscriptionOffer.Find(id);
            if (subscriptionOffer == null)
            {
                return HttpNotFound();
            }
            return View(subscriptionOffer);
        }

        // POST: SubscriptionOffers/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,beginDate,description,duration,endDate,name,price")] SubscriptionOffer subscriptionOffer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subscriptionOffer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(subscriptionOffer);
        }

        // GET: SubscriptionOffers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubscriptionOffer subscriptionOffer = db.SubscriptionOffer.Find(id);
            if (subscriptionOffer == null)
            {
                return HttpNotFound();
            }
            return View(subscriptionOffer);
        }

        // POST: SubscriptionOffers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubscriptionOffer subscriptionOffer = db.SubscriptionOffer.Find(id);
            db.SubscriptionOffer.Remove(subscriptionOffer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
