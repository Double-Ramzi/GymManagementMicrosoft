﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GymManagement.Filters;
using GymManagement.Models;

namespace GymManagement.Controllers
{
    [AuthActionFilter]
    public class MembersController : Controller
    {
        private Model1 db = new Model1();

        // GET: Members
        public ActionResult Index()
        {
            var member = db.Member.Include(m => m.Person);
            return View(member.ToList());
        }

        // GET: Members/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Member.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // GET: Members/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Person, "id", "address");
            return View();
        }

        // POST: Members/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                Person person = new Person();
                person.firstName = collection["Person.firstName"];
                person.lastName = collection["Person.lastName"];
                person.birthdate = new DateTime();
                person.gender = collection["Person.gender"];
                person.address = collection["Person.address"];
                person.email = collection["Person.email"];
                person.phoneNumber = collection["Person.phoneNumber"];
                person.phoneEmergency = collection["Person.phoneEmergency"];

                db.Person.Add(person);

                Member member = new Member();
                member.Person = person;
                
                db.Member.Add(member);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.id = new SelectList(db.Person, "id", "address", member.id);
            //return View(member);
            return View();
        }

        // GET: Members/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Member.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Person, "id", "address", member.id);
            return View(member);
        }

        // POST: Members/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, FormCollection collection)
        {
            Member member = db.Member.Find(id);

            if (ModelState.IsValid)
            {
                member.Person.firstName = collection["Person.firstName"];
                member.Person.lastName = collection["Person.lastName"];
                member.Person.gender = collection["Person.gender"];
                member.Person.birthdate = new DateTime();
                member.Person.email = collection["Person.email"];
                member.Person.phoneEmergency = collection["Person.phoneEmergency"];
                member.Person.phoneNumber = collection["Person.phoneNumber"];
                db.Entry(member).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id = new SelectList(db.Person, "id", "address", member.id);
            return View(member);
        }

        // GET: Members/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Member.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // POST: Members/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Member member = db.Member.Find(id);
            db.Member.Remove(member);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
