﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GymManagement.Filters;
using GymManagement.Models;

namespace GymManagement.Controllers
{
    [AuthActionFilter]
    public class SubscriptionsController : Controller
    {
        private Model1 db = new Model1();

        // GET: Subscriptions
        public ActionResult Index()
        {
            var subscription = db.Subscription.Include(s => s.Member).Include(s => s.SubscriptionOffer);
            return View(subscription.ToList());
        }

        // GET: Subscriptions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscription subscription = db.Subscription.Find(id);
            if (subscription == null)
            {
                return HttpNotFound();
            }
            return View(subscription);
        }

        // GET: Subscriptions/Create
        public ActionResult Create()
        {
            ViewBag.member_id = from m in db.Member select new SelectListItem { Value = m.id.ToString(), Text = m.Person.firstName + " " + m.Person.lastName };
            ViewBag.subscriptionOffer_id = new SelectList(db.SubscriptionOffer, "id", "name");
            return View();
        }

        // POST: Subscriptions/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,member_id,subscriptionOffer_id")] Subscription subscription)
        {
            if (ModelState.IsValid)
            {
                subscription.subscriptionDate = DateTime.Now;
                db.Subscription.Add(subscription);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.member_id = new SelectList(db.Member, "id", "id", subscription.member_id);
            ViewBag.subscriptionOffer_id = new SelectList(db.SubscriptionOffer, "id", "description", subscription.subscriptionOffer_id);
            return View(subscription);
        }

        // GET: Subscriptions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscription subscription = db.Subscription.Find(id);
            if (subscription == null)
            {
                return HttpNotFound();
            }
            ViewBag.member_id = from m in db.Member select new SelectListItem { Value = m.id.ToString(), Text = m.Person.firstName + " " + m.Person.lastName };
            ViewBag.subscriptionOffer_id = new SelectList(db.SubscriptionOffer, "id", "name", subscription.subscriptionOffer_id);
            return View(subscription);
        }

        // POST: Subscriptions/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,subscriptionDate,member_id,subscriptionOffer_id")] Subscription subscription)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subscription).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.member_id = from m in db.Member select new SelectListItem { Value = m.id.ToString(), Text = m.Person.firstName + " " + m.Person.lastName };
            ViewBag.subscriptionOffer_id = new SelectList(db.SubscriptionOffer, "id", "name", subscription.subscriptionOffer_id);
            return View(subscription);
        }

        // GET: Subscriptions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscription subscription = db.Subscription.Find(id);
            if (subscription == null)
            {
                return HttpNotFound();
            }
            return View(subscription);
        }

        // POST: Subscriptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subscription subscription = db.Subscription.Find(id);
            db.Subscription.Remove(subscription);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
