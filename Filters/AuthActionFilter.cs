﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GymManagement.Filters
{
    public class AuthActionFilter: ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var req = filterContext.HttpContext.Request.Cookies["user"];

            if (req != null)
            {
                return;
            }

            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}