﻿using GymManagement.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace GymManagement.Manager
{
    public class UserManager
    {
        private Model1 storage = new Model1();
        private PasswordHasher hasher = new PasswordHasher();

        public UserManager(Model1 storage)
        {
            this.storage = storage;
        }

        public Account Validate(string identifier, string secret)
        {
            Account account = storage.Account.Where(
                a =>  a.userName == identifier
            ).FirstOrDefault();

            if (account != null)
            {
                PasswordVerificationResult result = hasher.VerifyHashedPassword(account.password, secret);
                if (result == PasswordVerificationResult.Success)
                {
                    return account;
                }
            }

            return null;
        }

        public Account Register(Account account)
        {
            account.password = hasher.HashPassword(account.password);

            storage.Account.Add(account);
            storage.SaveChanges();

            return account;
        }

        public void SignIn(HttpContext httpContext, Account account, bool isPersistent = false)
        {
            //create a cookie
            HttpCookie myCookie = new HttpCookie("user");

            //Add key-values in the cookie
            myCookie.Values.Add("userid", account.id.ToString());

            //set cookie expiry date-time. Made it to last for next 12 hours.
            if (!isPersistent)
                myCookie.Expires = DateTime.Now.AddHours(12);

            //Most important, write the cookie to client.
            httpContext.Response.Cookies.Add(myCookie);
        }

        public void SignOut(HttpContext httpContext)
        {
            httpContext.Session.Abandon();
            httpContext.Response.Cookies["user"].Expires = DateTime.Now.AddDays(-1);
        }
    }
}